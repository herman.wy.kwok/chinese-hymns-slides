# Chinese Hymns Slides

## What is Chinese Hymns Slides?
Chinese Hymns Slides is a collection of slides, coded in static HTML format, with [reveal.js](https://github.com/hakimel/reveal.js) as the presentation library, of lyrics of various Christians hymns in Chinese Languages. 

The primary usage for the slides are for personnel in control panel to project lyrics on screen when choir or band is playing music or singing hymns. The target viewer of the slides are the worshiping people.

The Chinese Hymns Slides are aimed to be displayed on computers with low hardware requirements, and with or without internet connection.

Please check my [demo](https://slides.com/hermankwok/cp498-1/live#/)

## How to install it?
Just download the zip file and extract it. Slides are not a program. A web browser is all you need.

## How to use Chinese Hymns Slides?
1. download the zip file
2. unzip the zip file
3. open index.html in a web browser

That's all.

## What are the limitation of Chinese Hymns Slides?
At present, The slides has following limitations
1. Single langauage (Traditional Chinese)
2. No chord was shown
3. No database are used to store songs
4. No meta information such as chord included, (except, for each hymns, a slide is reserved for author and copyright)
5. No synchronization when two or more people using the slides.

## Project goals
1. Chinese Hymns Slides at present contains some hymns from Century Praise. More hymns collections will be added.
2. Static site generator is planned to use generate slides for hymns collections
3. Another project which contains a management system is in plannng stage for overcome various limitation of Chinese Hymns Slides

## Want to contribute?
Please feel free to contact me via herman (dot) wy (dot) kwok (at) gmail (dot) com if you find any mistakes in the slides, or if you have any idea about this project, or the management system for the next project. All comments and suggestions are welcome.

## Legal 
- Chinese Hymns Slides is currently available under [Creative Common NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/). Check that out or see LICENSE.txt for more information.
- [reveal.js](https://github.com/hakimel/reveal.js) is availble under [MIT License](https://en.wikipedia.org/wiki/MIT_License) by [Hakim El Hattab](http://hakim.se)
- Century Praise 世紀頌讚 is copyrighted by [Chinese Baptist Press (International)](http://www.bappress.org/)

## About me
A Chinese Christian Coder.

(e):<herman.wy.kwok@gmail.com>.
